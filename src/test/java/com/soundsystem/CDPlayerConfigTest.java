package com.soundsystem;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CDPlayerConfig.class)
public class CDPlayerConfigTest {

	@Autowired
	CompactDisk cd;

	@Test
	public void CompactDiskShouldNotBeNull(){
		assertNotNull(cd);
	}
	@Test
	public void contextLoads() {



	}

}
