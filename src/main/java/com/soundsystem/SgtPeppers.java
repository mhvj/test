package com.soundsystem;

import org.springframework.stereotype.Component;

/**
 * Created by mv on 14/10/2015.
 */

@Component
public class SgtPeppers implements CompactDisk {
    private String title="Song title";
    private String artist="John Artist";

    @Override
    public void play() {
        System.out.println("Playing "+title+"by "+artist);
    }
}
