package com.soundsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CDPlayerConfig {

    public static void main(String[] args) {
        SpringApplication.run(CDPlayerConfig.class, args);
    }
}
